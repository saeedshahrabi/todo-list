import { todoType } from "../../types/todoTypes";
import { Ul } from "./index-style";
import TodoItem from "./todoItem";


interface IProps{
    items : Array<todoType>;
    onItemCompleted:(e:number)=>void;
    onDeleteItem:(e:number)=>void;
}

const TodoList:React.FunctionComponent<IProps> = (props) => {
    return (
      <Ul className="todolist">
        {props.items.map((item) => (
          <TodoItem
            key={item.id}
            id={item.id}
            text={item.text}
            completed={item.done}
            onItemCompleted={props.onItemCompleted}
            onDeleteItem={props.onDeleteItem}
          />
        ))}
      </Ul>
    );
  };
export default TodoList  