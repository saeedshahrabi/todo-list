import styled from "styled-components";

export const H3 = styled.h3`
  margin: 1rem 0;
  color: #ff2968;
  font-size: 1.75rem;
`;
