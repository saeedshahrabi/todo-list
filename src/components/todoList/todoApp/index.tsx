import { useState } from "react";
import TodoList from "..";
import { todoType } from "../../../types/todoTypes";
import { H3 } from "./todoApp-style";


const TodoApp = () => {
  const [items, setItems] = useState<Array<todoType>>(
    localStorage.getItem("todo")
      ? JSON.parse(localStorage.getItem("todo") as string)
      : []
  );
  const [text, setText] = useState<string>("");

  const handleTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };

  const handleAddItem = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();

    const newItem = {
      id: Date.now(),
      text: text,
      done: false,
    };
    setText("");
    setItems((prevState) => {
      localStorage.setItem("todo", JSON.stringify(prevState.concat(newItem)));
      return prevState.concat(newItem);
    });
  };
  const markItemCompleted = (itemId: number) => {
    const updatedItems = items.map((item) => {
      if (itemId === item.id) item.done = !item.done;
      return item;
    });
    localStorage.setItem(
      "todo",
      JSON.stringify([].concat(updatedItems as any))
    );
    // State Updates are Merged
    setItems([].concat(updatedItems as any));
  };
  const handleDeleteItem = (itemId: number) => {
    const updatedItems = items.filter((item) => {
      return item.id !== itemId;
    });
    localStorage.setItem(
      "todo",
      JSON.stringify([].concat(updatedItems as any))
    );
    setItems([].concat(updatedItems as any));
  };
  const handleDeleteSelected=()=>{
   const updatedItems = items.filter(item=>!item.done)
   localStorage.setItem(
    "todo",
    JSON.stringify([].concat(updatedItems as any))
  );
  setItems([].concat(updatedItems as any));
  }
  return (
    <div className="container">
      <H3>TO DO LIST</H3>
      <div className="row">
        <div className="col-12">
            <button className="btn btn-danger mb-3" disabled={false} onClick={handleDeleteSelected} >delete</button>
        </div>
        <div className="col-md-3">
          <TodoList
            items={items}
            onItemCompleted={markItemCompleted}
            onDeleteItem={handleDeleteItem}
          />
        </div>
      </div>
      <form className="row">
        <div className="col-md-3">
          <input
            type="text"
            className="form-control"
            onChange={handleTextChange}
            value={text}
          />
        </div>
        <div className="col-md-3">
          <button
            className="btn btn-primary"
            onClick={handleAddItem}
            disabled={!text}
          >
            {"Add #" + (items.length + 1)}
          </button>
        </div>
      </form>
    </div>
  );
};
export default TodoApp;
