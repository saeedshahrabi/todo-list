import { DeleteItem, Label, Li } from "./todoItem-style";


interface IProps {
  onItemCompleted: (e: number) => void;
  onDeleteItem: (e: number) => void;
  id: number;
  completed: boolean;
  text: string;
}

const TodoItem: React.FunctionComponent<IProps> = (props) => {
  const markCompleted = () => {
    props.onItemCompleted(props.id);
  };
  const deleteItem = () => {
    props.onDeleteItem(props.id);
  };
  // Highlight newly added item for several seconds.

  return (
    <Li>
      <Label
      style={ props.completed ? {textDecoration:"line-through"}: {}}
      className="form-check-label">
        <input
          type="checkbox"
          className="form-check-input"
          onChange={markCompleted}
          checked={props.completed}
        />{" "}
        {props.text}
      </Label>
      <DeleteItem
        type="button"
        className="btn btn-danger btn-sm"
        onClick={deleteItem}
      >
        x
      </DeleteItem>
    </Li>
  );
};

export default TodoItem;
