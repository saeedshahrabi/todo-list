import styled from "styled-components";

export const Li = styled.li`
position: relative;
margin-bottom: 0.25rem;
padding: 0.5rem 2rem 0.5rem 0.5rem;
border-top: 1px solid #ccc;
list-style: none;
&:last-child {
  border-bottom: 1px solid #ccc;
}
`;
export const DeleteItem = styled.button`
position: absolute;
top: 0.5rem;
right: 0.5rem;
`;
export const Label = styled.label`
width:100%;
cursor: pointer;
`;